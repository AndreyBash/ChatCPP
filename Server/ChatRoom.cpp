
#include "ChatRoom.h"
#include "iostream"

void ChatRoom::join(IChatSessionPtr participant)
{
    participants_.insert(participant);
    for (auto msg: recentMsgs_)
        participant->deliver(msg);
    std::cout << "Client join room (total = " << participants_.size() << ")" << std::endl;
    ChatMessage msg("Server", "New guest join room");
    deliver(msg);
}

void ChatRoom::leave(IChatSessionPtr participant)
{
    participants_.erase(participant);
    std::cout << "Client leave room (total = " << participants_.size() << ")" << std::endl;
    ChatMessage msg("Server", "Guest leave room");
    deliver(msg);
}

void ChatRoom::deliver(const ChatMessage& msg)
{
    recentMsgs_.push_back(msg);
    while (recentMsgs_.size() > max_recent_msgs)
        recentMsgs_.pop_front();

    for (auto participant: participants_)
        participant->deliver(msg);
}