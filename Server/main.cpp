#include <cstdlib>
#include <iostream>
#include "ChatServer.h"

int main(int argc, char* argv[])
{
    try
    {
        std::string port = "40003";

        if (argc == 2)
        {
            port = argv[1];
        }

        ChatServer server(port);
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}