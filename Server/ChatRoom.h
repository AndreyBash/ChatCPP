#ifndef SERVER_CHAT_ROOM_H
#define SERVER_CHAT_ROOM_H

#include <set>
#include <deque>
#include "ChatMessage.h"
#include "IChatSession.h"

typedef std::deque<ChatMessage> ChatMessageQueue;

class ChatRoom
{
public:
    virtual ~ChatRoom(){};

    void join(IChatSessionPtr participant);
    void leave(IChatSessionPtr participant);
    void deliver(const ChatMessage& msg);

private:
    std::set<IChatSessionPtr> participants_;
    enum { max_recent_msgs = 100 };
    ChatMessageQueue recentMsgs_;
};


#endif //SERVER_CHAT_ROOM_H
