
#include "ChatServer.h"
#include "AsioServerSocketAdapter.h"

ChatServer::ChatServer(std::string port){
    std::make_shared<AsioServerSocketAdapter>(port, std::bind(&ChatServer::onAcceptConnecton, this, std::placeholders::_1));
}

void ChatServer::onAcceptConnecton(IChatSessionPtr sessionPtr)
{
    sessionPtr.get()->start(std::bind(&ChatServer::onSessionLeave,this, std::placeholders::_1), std::bind(&ChatServer::onSessionRead,this, std::placeholders::_1));
    room_.join(sessionPtr);
}

void ChatServer::onSessionLeave(IChatSessionPtr sessionPtr)
{
    room_.leave(sessionPtr);
}

void ChatServer::onSessionRead(const ChatMessage& msg)
{
    room_.deliver(msg);
}