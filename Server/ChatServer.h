
#ifndef SERVER_CHAT_SERVER_H
#define SERVER_CHAT_SERVER_H

#include "ChatRoom.h"

class ChatServer
{
public:
    ChatServer(std::string port);
    virtual ~ChatServer(){};

private:
    void onAcceptConnecton(IChatSessionPtr sessionPtr);
    void onSessionLeave(IChatSessionPtr sessionPtr);
    void onSessionRead(const ChatMessage& msg);

private:
    ChatRoom room_;
};


#endif //SERVER_CHAT_SERVER_H
