#include "AsioServerSocketAdapter.h"

AsioServerSocketAdapter::AsioServerSocketAdapter(std::string port, std::function<void(IChatSessionPtr)> acceptCallback):
        io_service_(), socket_(io_service_),endpoint_(tcp::v4(), std::atoi(port.c_str())),acceptor_(io_service_, endpoint_)
{
    acceptCallback_ = acceptCallback;
    doAsyncAccept();
    io_service_.run();
};

void AsioServerSocketAdapter::doAsyncAccept()
{
    acceptor_.async_accept(socket_,
                           [this](std::error_code ec)
                           {
                               if (!ec)
                               {
                                   acceptCallback_(std::make_shared<AsioChatSession>(std::move(socket_)));
                               }

                               doAsyncAccept();
                           });
};
