#include "AsioChatSession.h"

void AsioChatSession::start(std::function<void(IChatSessionPtr)> leaveCallback, std::function<void(const ChatMessage&)> readCallback)
{
    leaveCallback_ = leaveCallback;
    readCallback_ = readCallback;
    readHeader();
}

void AsioChatSession::deliver (const ChatMessage& msg)
{
    bool write_in_progress = !write_msgs_.empty();
    write_msgs_.push_back(msg);
    if (!write_in_progress)
    {
        write();
    }
}

void AsioChatSession::readHeader()
{
    auto self(shared_from_this());
    asio::async_read(socket_,
                     asio::buffer(read_msg_.data(), ChatMessage::HEADER_LENGTH),
                     [this, self](std::error_code ec, std::size_t /*length*/)
                     {
                         if (!ec && read_msg_.decode_header())
                         {
                             readBody();
                         }
                         else
                         {
                             leaveCallback_(shared_from_this());
                         }
                     });
}

void AsioChatSession::readBody()
{
    auto self(shared_from_this());
    asio::async_read(socket_,
                     asio::buffer(read_msg_.body(), read_msg_.bodyLength()),
                     [this, self](std::error_code ec, std::size_t /*length*/)
                     {
                         if (!ec)
                         {
                             readCallback_(read_msg_);
                             readHeader();
                         }
                         else
                         {
                             leaveCallback_(shared_from_this());
                         }
                     });
}

void AsioChatSession::write()
{
    auto self(shared_from_this());
    asio::async_write(socket_,
                      asio::buffer(write_msgs_.front().data(),
                                   write_msgs_.front().length()),
                      [this, self](std::error_code ec, std::size_t /*length*/)
                      {
                          if (!ec)
                          {
                              write_msgs_.pop_front();
                              if (!write_msgs_.empty())
                              {
                                  write();
                              }
                          }
                          else
                          {
                              leaveCallback_(shared_from_this());
                          }
                      });
}