
#ifndef SOCKETIMPL_ASIOCHATSESSION_H
#define SOCKETIMPL_ASIOCHATSESSION_H

#define ASIO_STANDALONE
#include "asio.hpp"
#include <memory>
#include <iostream>
#include <deque>
#include "../../ChatMessage.h"
#include "../../IChatSession.h"

using asio::ip::tcp;

typedef std::deque<ChatMessage> ChatMessageQueue;

class AsioChatSession: public IChatSession, public std::enable_shared_from_this<IChatSession>
{
public:
    AsioChatSession(tcp::socket socket):socket_(std::move(socket)){};

    virtual ~AsioChatSession() {};

    void start(std::function<void(IChatSessionPtr)> leaveCallback, std::function<void(const ChatMessage&)> readCallback) override;
    void deliver (const ChatMessage& msg) override;

private:
    void readHeader();
    void readBody();
    void write();

    tcp::socket socket_;
    ChatMessage read_msg_;
    ChatMessageQueue write_msgs_;
    std::function<void(IChatSessionPtr)> leaveCallback_;
    std::function<void(const ChatMessage&)> readCallback_;
};

#endif //SOCKETIMPL_ASIOCHATSESSION_H
