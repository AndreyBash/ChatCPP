#ifndef SOCKETIMPL_ASIOCLIENTSOCKETADAPTER_H
#define SOCKETIMPL_ASIOCLIENTSOCKETADAPTER_H

#define ASIO_STANDALONE

#include <iostream>
#include <deque>
#include <thread>
#include "asio.hpp"
#include "../../ChatMessage.h"
#include "../../IClientSocketAdapter.h"

using asio::ip::tcp;

typedef std::deque<ChatMessage> ChatMessageQueue;

class AsioClientSocketAdapter: public IClientSocketAdapter {
public:
    AsioClientSocketAdapter(std::string host, std::string port, void (*readCallback)(ChatMessage msg));
    virtual ~AsioClientSocketAdapter() {};

    void send(ChatMessage const & msg) override;
    void close() override;

private:
    void connect();
    void readHeader();
    void readBody();
    void write();

private:
    asio::io_service io_service_;
    tcp::socket socket_;
    std::thread thread_;
    tcp::resolver::iterator endpointIterator_;
    ChatMessage curReadMsg_;
    ChatMessageQueue writeMsgs_;
    void (*readCallback_)(ChatMessage msg);
};


#endif //SOCKETIMPL_ASIOCLIENTSOCKETADAPTER_H
