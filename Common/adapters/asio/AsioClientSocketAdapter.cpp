
#include "AsioClientSocketAdapter.h"

AsioClientSocketAdapter::AsioClientSocketAdapter(std::string host, std::string port, void (*readCallback)(ChatMessage msg)):
io_service_(),socket_(io_service_), readCallback_(readCallback){
    tcp::resolver resolver(io_service_);
    endpointIterator_ = resolver.resolve({ host, port });
    connect();
    thread_ = std::thread([this](){ io_service_.run(); });
};

void AsioClientSocketAdapter::send(ChatMessage const & msg) {
    io_service_.post(
            [this, msg]() {
                bool write_in_progress = !writeMsgs_.empty();
                writeMsgs_.push_back(msg);
                if (!write_in_progress) {
                    write();
                }
            });
}

void AsioClientSocketAdapter::close() {
    io_service_.post([this]() { socket_.close(); });
    thread_.join();
}

void AsioClientSocketAdapter::connect() {
    asio::async_connect(socket_, endpointIterator_,
                        [this](std::error_code ec, tcp::resolver::iterator) {
                            if (!ec)
                                readHeader();
                            else {
                                std::cout << "Connection error: " << ec << " Trying to restart connection in 3 seconds..." << std::endl;
                                std::this_thread::sleep_for(std::chrono::seconds(3));
                                connect();
                            }
                        });
};

void AsioClientSocketAdapter::readHeader()
{
    asio::async_read(socket_,
                     asio::buffer(curReadMsg_.data(), ChatMessage::HEADER_LENGTH),
                     [this](std::error_code ec, std::size_t /*length*/)
                     {
                         if (!ec && curReadMsg_.decode_header())
                         {
                             readBody();
                         }
                         else
                         {
                             socket_.close();
                             std::cout << "Read header error: " << ec << "  Trying to restart connection in 3 seconds..." << std::endl;
                             std::this_thread::sleep_for(std::chrono::seconds(3));
                             connect();
                         }
                     });
};

void AsioClientSocketAdapter::readBody()
{
    asio::async_read(socket_,
                     asio::buffer(curReadMsg_.body(), curReadMsg_.bodyLength()),
                     [this](std::error_code ec, std::size_t /*length*/)
                     {
                         if (!ec)
                         {
                             readCallback_(curReadMsg_);
                             readHeader();
                         }
                         else
                         {
                             socket_.close();
                             std::cout << "Read body error: " << ec << "  Trying to restart connection in 3 seconds..." << std::endl;
                             std::this_thread::sleep_for(std::chrono::seconds(3));
                             connect();
                         }
                     });
};

void AsioClientSocketAdapter::write()
{
    asio::async_write(socket_,
                      asio::buffer(writeMsgs_.front().data(),
                                   writeMsgs_.front().length()),
                      [this](std::error_code ec, std::size_t /*length*/)
                      {
                          if (!ec)
                          {
                              writeMsgs_.pop_front();
                              if (!writeMsgs_.empty())
                              {
                                  write();
                              }
                          }
                          else
                          {
                              socket_.close();
                              std::cout << "Write error: " << ec << "  Trying to restart connection in 3 seconds..." << std::endl;
                              std::this_thread::sleep_for(std::chrono::seconds(3));
                              connect();
                          }
                      });
};