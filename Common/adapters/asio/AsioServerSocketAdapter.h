#ifndef SOCKETIMPL_ASIOSERVERSOCKETADAPTER_H
#define SOCKETIMPL_ASIOSERVERSOCKETADAPTER_H

#define ASIO_STANDALONE
#include "asio.hpp"
#include <functional>
#include "AsioChatSession.h"
#include "../../IServerSocketAdapter.h"

using asio::ip::tcp;

class AsioServerSocketAdapter: public IServerSocketAdapter {
public:
    AsioServerSocketAdapter(std::string port, std::function<void(IChatSessionPtr)> acceptCallback);
    ~AsioServerSocketAdapter(){};

private:
    void doAsyncAccept();
private:
    asio::io_service io_service_;
    tcp::socket socket_;
    tcp::endpoint endpoint_;
    tcp::acceptor acceptor_;
    std::function<void(IChatSessionPtr)> acceptCallback_;
};


#endif //SOCKETIMPL_ASIOSERVERSOCKETADAPTER_H
