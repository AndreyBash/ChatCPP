//
// Created by Andrew Bashmakov on 22/09/2017.
//

#ifndef SOCKETIMPL_ISERVERSOCKETADAPTER_H
#define SOCKETIMPL_ISERVERSOCKETADAPTER_H

#include <cstdlib>

class IServerSocketAdapter {
public:
    IServerSocketAdapter(){};
    IServerSocketAdapter(IServerSocketAdapter const & other) = delete;
    IServerSocketAdapter&operator=(IServerSocketAdapter const & other) = delete;
    virtual ~IServerSocketAdapter(){};
};

#endif //SOCKETIMPL_ISERVERSOCKETADAPTER_H
