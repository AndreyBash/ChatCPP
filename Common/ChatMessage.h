#ifndef CHAT_MESSAGE_H
#define CHAT_MESSAGE_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>

class ChatMessage
{
public:
    static const size_t HEADER_SENDER_LENGTH = 10;
    static const size_t HEADER_BODY_LENGTH = 4;
    static const size_t HEADER_LENGTH = HEADER_SENDER_LENGTH + HEADER_BODY_LENGTH;
    static const size_t MAX_BODY_LENGTH = 512;

    ChatMessage() : bodyLength_(0) {}
    ChatMessage(char const * sender, char const * text):bodyLength_(std::strlen(text)){
        encode_header(sender, bodyLength_);
        std::memcpy(body(), text, bodyLength_);
    }

    ~ChatMessage(){};

    const char* data() const
    {
        return data_;
    }

    char* data()
    {
        return data_;
    }

    const char* body() const
    {
        return data_ + HEADER_LENGTH;
    }

    char* body()
    {
        return data_ + HEADER_LENGTH;
    }

    std::size_t bodyLength() const
    {
        return bodyLength_;
    }

    std::size_t length() const
    {
        return HEADER_LENGTH + bodyLength_;
    }

    std::string const & sender() const
    {
        return sender_;
    }

    bool decode_header()
    {
        sender_ = std::string(data_, data_+ HEADER_SENDER_LENGTH);
        sender_.erase(sender_.begin(), std::find_if(sender_.begin(), sender_.end(), [](int ch) {
            return !std::isspace(ch);
        }));

        char bLength[HEADER_LENGTH + 1] = "";
        std::strncat(bLength, data_ + HEADER_SENDER_LENGTH, HEADER_BODY_LENGTH);
        bodyLength_ = std::atoi(bLength);
        if (bodyLength_ > MAX_BODY_LENGTH)
        {
            bodyLength_ = 0;
            return false;
        }
        return true;
    }

private:
    void encode_header(char const * sender, std::size_t bodyLength)
    {
        char sndr[HEADER_SENDER_LENGTH + 1] = "";
        std::sprintf(sndr, "%10s", sender);
        std::memcpy(data_, sndr, HEADER_SENDER_LENGTH);
        char bLength[HEADER_BODY_LENGTH + 1] = "";
        std::sprintf(bLength, "%4d", static_cast<int>(bodyLength));
        std::memcpy(data_ + HEADER_SENDER_LENGTH, bLength, HEADER_BODY_LENGTH);
    }

private:
    char data_[HEADER_LENGTH + MAX_BODY_LENGTH];
    std::size_t bodyLength_;
    std::string sender_;
};

#endif // CHAT_MESSAGE_H
