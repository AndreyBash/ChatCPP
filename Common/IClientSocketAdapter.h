#ifndef SOCKETIMPL_ISOCKETADAPTER_H
#define SOCKETIMPL_ISOCKETADAPTER_H

#include <cstdlib>
#include "ChatMessage.h"

class IClientSocketAdapter {
public:
    IClientSocketAdapter(){};
    IClientSocketAdapter(IClientSocketAdapter const & other) = delete;
    IClientSocketAdapter&operator=(IClientSocketAdapter const & other) = delete;
    virtual ~IClientSocketAdapter(){};
    virtual void send(ChatMessage const & msg) = 0;
    virtual void close() = 0;
};

#endif //SOCKETIMPL_ISOCKETADAPTER_H
