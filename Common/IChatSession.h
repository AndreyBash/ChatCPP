#ifndef SOCKETIMPL_ICHATPARTICIPANT_H
#define SOCKETIMPL_ICHATPARTICIPANT_H

#include <functional>
#include <memory>
#include "ChatMessage.h"
#include "IChatSession.h"

class IChatSession;
typedef std::shared_ptr<IChatSession> IChatSessionPtr;

class IChatSession {
public:
    IChatSession(){};
    virtual ~IChatSession() {};
    IChatSession(IChatSession const & other) = delete;
    IChatSession&operator=(IChatSession const & other) = delete;
    virtual void start(std::function<void(IChatSessionPtr)> leaveCallback, std::function<void(const ChatMessage&)> readCallback) = 0;
    virtual void deliver(const ChatMessage& msg) = 0;
};

#endif //SOCKETIMPL_ICHATPARTICIPANT_H
