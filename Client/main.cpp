#include <cstdlib>
#include <iostream>
#include "ChatMessage.h"
#include "IClientSocketAdapter.h"
#include "AsioClientSocketAdapter.h"

using namespace std;

string clientName_;

void printMessage(ChatMessage msg){
    if (clientName_ != msg.sender())
        printf("%s : %.*s \n", msg.sender().c_str(), (int)msg.bodyLength(), msg.body());
};

int main(int argc, char* argv[])
{
    try
    {
        srand (time(NULL));
        clientName_ = "Guest_" + std::to_string(rand()%100);
        string host = "localhost";
        string port = "40003";

        if (argc == 3)
        {
            host = argv[1];
            port = argv[2];
        }

        shared_ptr<IClientSocketAdapter> client = std::make_shared<AsioClientSocketAdapter>(host, port, printMessage);

        char line[ChatMessage::MAX_BODY_LENGTH + 1];
        while (cin.getline(line, ChatMessage::MAX_BODY_LENGTH + 1))
        {
            ChatMessage msg(clientName_.c_str(), line);
            client.get()->send(msg);
        }
        client.get()->close();
    }
    catch (std::exception& e)
    {
        cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}